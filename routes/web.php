<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('arreglo' , function(){

    //crear un arreglo de estudiantes

    $estudiantes = [ "AD" => "andres" ,
    "LA" => "Laura" ,
    "ANN" =>"ana" , 
    "RO" => "rodrigo"  ];
    //var_dump($estudiantes);

    //Recorrer un arreglo
    foreach ($estudiantes as $indice => $estudiante){
        echo "$estudiante tiene indice  $indice <hr />";
    }

});
Route :: get ('paises' , function(){
    //cfrear un arreglo con informacion de paises
    $paises=[
        "COLOMBIA" =>[
            "capital"=>"Bogota",
            "moneda"=>"peso",
            "peso"=>50.372
 ],
        "BOLIVIA"=>[ 
            "capital"=>"Quito",
            "moneda"=>"Dolar",
            "peso"=>17.517
         ],
        "BRASIL"=>[
         "capital"=>"Brazilia",
         "moneda"=>"Dolar",
         "peso"=>212.216
        ],
        "ECUADOR"=>[
        "capital"=>"Quito",
         "moneda"=>"Real",
         "peso"=>212.213
        ],
    ];

    // Recorrer la primera dimension del areglo
    //foreach ($paises as $pais => $infopais ){
       // echo "<h2> $pais </h2>";
        //echo "capital:" . $infopais ["capital"] . "<br/>";
        //echo "moneda:" . $infopais ["moneda"] . "<br/>"; ;
        //echo "poblacion (en millones de habitantes)" . $infopais ["poblacion"] . "<br />";
        //echo "<hr />";
    //}
    //echo"<pre>";
   // var_dump($paises);
    //echo"</pre>";




    // mostrar una vista para presentar los paises
    //En MVC yo puedo pasar datos a una vista

    return view ('paises') ->with ("paises" , $paises);
 });
  
 




  
